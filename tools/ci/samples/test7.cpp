#include "test7.h"

#ifndef QQQ
#include "include1.h"
#else  // QQQ
#include "include2.h"
#endif  // QQQ

int function1()
{
    return 0;
}

#ifdef QQQ
#include "include3.h"
#endif  // QQQ
