#ifndef TEST7
#define TEST7

int function1();

#ifndef QQQ
#include "include1.h"
#else  // QQQ
#include "include2.h"
#endif  // QQQ

#ifdef QQQ
#include "include3.h"
#endif  // QQQ

#endif  // TEST7