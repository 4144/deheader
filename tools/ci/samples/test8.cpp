#include "test8.h"

#if !defined(ZZZ)
#ifndef QQQ
#include "include1.h"
#else  // QQQ
#include "include2.h"
#endif  // QQQ
#endif  // !defined(ZZZ)

int function1()
{
    return 0;
}

#if defined(QQQ)
#include "include3.h"
#endif  // defined(QQQ)
