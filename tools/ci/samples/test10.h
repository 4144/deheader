#ifndef TEST10
#define TEST10

#ifndef QQQ
#include "include2.h"
#elif !defined(ZZZ)
#include "include4.h"
#endif  // QQQ

int function1();

#define ZZZ
#endif  // TEST10
