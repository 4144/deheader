#ifndef TEST12
#define TEST12

#include <map>
#include <string>

class Class1
{
    Class1(const std::string &str) :
        mStr(str)
    {
    }
    const std::string mStr;
};

#endif  // TEST12
